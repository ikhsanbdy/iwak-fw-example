'use strict';

/**
 *
 *
 */

const Database  = use('Database');

module.exports = Database.model('Category', {
	tableName: 'categories',
	hasTimestamps: true,
	hidden: ['created_by', 'updated_by', 'deleted_by', 'deleted_at'],
	softDelete: true,
	products: function () {
		return this.hasMany('Product');
	}
});