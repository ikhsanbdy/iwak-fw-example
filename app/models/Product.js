'use strict';

/**
 *
 *
 */

const Database  = use('Database');

const Discount = use('/app/models/Discount');

module.exports = Database.model('Product', {
	tableName: 'products',
	hasTimestamps: true,
	hidden: ['created_by', 'updated_by', 'deleted_by', 'deleted_at'],
	softDelete: true,
	discount: function () {
		return this.belongsTo('Discount');
	},
	category: function () {
		return this.belongsTo('Category');
	}
});