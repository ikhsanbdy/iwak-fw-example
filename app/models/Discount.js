'use strict';

/**
 *
 *
 */

const Database  = use('Database');

module.exports = Database.model('Discount', {
	tableName: 'discounts',
	hasTimestamps: true,
	hidden: ['created_by', 'updated_by', 'deleted_by', 'deleted_at'],
	softDelete: true
});