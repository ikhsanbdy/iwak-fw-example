'use strict';

/**
 *
 *
 */

const Route = use('Route');

Route.group({prefix: '/api', namespace: 'api'}, (Route) => {

	Route.resource({prefix: '/categories', param: 'id(\\d)'}, 'CategoryController');

	// Route.resource({prefix: '/discounts', param: 'id(\\d)'}, 'DiscountController');

	Route.resource({prefix: '/products', param: 'id(\\d)'}, 'ProductController');

}).error('json');