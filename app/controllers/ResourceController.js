'use strict';

/**
 *
 *
 */

const Validator = use('Validator');
const Response 	= use('Response');
const Paginator = use('/app/libraries/Paginator');

class ResourceController {

	/**
	 *
	 *
	 */
	constructor(model = null, fetchOptions, resources) {
		this.model = model;
		this.fetchOptions = fetchOptions || {index: {}, show: {}};
		this.resources = resources || ['index', 'show', 'store', 'update', 'destroy'];
		this.orderableColumns = ['created_at', '-created_at'];
	}

	/**
	 *
	 *
	 */
	index(req, res, next) {
		try {
			const rules = {
				'page': 'integer',
				'page_size': 'integer',
				'order': `in:${this.orderableColumns}`,
				'id': 'match:^[\\d\\,]+$'
			};

			var Input = Validator.query(req, rules);

			if(Input.error()) {
				return Response.unprocessableEntity(res, 422001, 'Validation error', Input.messages());
			}

			var pagination = {
				page: Input.get('page', 1),
				pageSize: Input.get('page_size', 10)
			};
		}
		catch(err) {
			return next(err);
		}

		var Model = this.model.forge();

		if(Input.get('id')) {
			Model.query((qb) => {
				qb.whereIn('id', Input.get('id').split(',').filter(Number))
			});
		}

		return Model.orderBy(Input.get('order', 'created_at'))
		.fetchPage(Object.assign(this.fetchOptions.index, pagination))
		.then((data) => {
			return Response.ok(res, Paginator.build(data));
		})
		.catch((err) => {
			return next(err);
		});
	}

	/**
	 *
	 *
	 */
	show(req, res, next) {
		return this.model.forge(req.params).fetch(this.fetchOptions.show)
		.then((data) => {
			if(!data) return Response.notFound(res, 404001, 'Not found');

			return Response.ok(res, data);
		})
		.catch((err) => {
			return next(err);
		});
	}

	/**
	 *
	 *
	 */
	store(req, res, next) {
		try {
			var Input = Validator.body(req, this.storeValidation || this.validations || {});

			if(Input.error()) {
				return Response.unprocessableEntity(res, 422001, 'Validation error', Input.messages());
			}
		}
		catch(err) {
			return next(err);
		}

		return new this.model().save(Input.get())
		.then((data) => {
			return Response.created(res, data);
		}).catch((err) => {
			return next(err);
		});
	}

	/**
	 *
	 *
	 */
	update(req, res, next) {
		try {
			var Input = Validator.body(req, this.updateValidation || this.validations || {});

			if(Input.error()) {
				return Response.unprocessableEntity(res, 422001, 'Validation error', Input.messages());
			}
		}
		catch(err) {
			return next(err);
		}

		return this.model.forge(req.params).save(Input.get())
		.then((data) => {
			return Response.ok(res, data);
		}).catch((err) => {
			if(err.message == 'No Rows Updated') return Response.notFound(res, 404001, 'Not found');

			return next(err);
		});
	}

	/**
	 *
	 *
	 */
	destroy(req, res, next) {
		return this.model.forge(req.params).destroy()
		.then((data) => {
			return Response.ok(res, data);
		}).catch((err) => {
			return next(err);
		});
	}
}

module.exports = ResourceController;