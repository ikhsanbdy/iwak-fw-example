'use strict';

/**
 *
 *
 */

const Validator = use('Validator');
const Response 	= use('Response');

const Product 	= use('/app/models/Product');

const ResourceController = use('/app/controllers/ResourceController');

class ProductController extends ResourceController {

	/**
	 * Constructor
	 */
	constructor() {
		super(Product);

		this.fetchOptions.index = {
			withRelated: ['discount', 'category']
		};

		this.fetchOptions.show = {
			withRelated: ['discount', 'category']
		};

		this.storeValidation = {
			'name': 'required|string|min:3',
			'code': 'required|string|min:3',
			'price': 'required|numeric',
			'category_id': 'required|integer',
			'discount_id': 'integer',
			'activated': 'required|boolean',
			'description': 'string',
			'stock_number': 'integer'
		};

		this.updateValidation = {
			'name': 'string|min:3',
			'code': 'string|min:3',
			'price': 'numeric',
			'category_id': 'integer',
			'discount_id': 'integer',
			'activated': 'required|boolean',
			'description': 'string',
			'stock_number': 'integer'
		};
	}

	/**
	 * @api {get} /products Product list
	 * @apiVersion 1.0.0
	 * @apiName ListProduct
	 * @apiGroup Product
	 * @apiDescription Read all product list
	 *
	 */
	index(req, res, next) {
		super.index(req, res, next);
	}

	/**
	 * @api {get} /products/:id Product detail
	 * @apiVersion 1.0.0
	 * @apiName DetailProduct
	 * @apiGroup Product
	 * @apiDescription Read product detail
	 *
	 */
	show(req, res, next) {
		super.show(req, res, next);
	}

	/**
	 * @api {post} /products Create product
	 * @apiVersion 1.0.0
	 * @apiName CreateProduct
	 * @apiGroup Product
	 * @apiDescription Create new product
	 * @apiPermission admin
	 *
	 */
	store(req, res, next) {
		super.store(req, res, next);
	}

	/**
	 * @api {put} /products/:id Update product
	 * @apiVersion 1.0.0
	 * @apiName UpdateProduct
	 * @apiGroup Product
	 * @apiDescription Update Product
	 * @apiPermission admin
	 *
	 *
	 */
	update(req, res, next) {
		super.update(req, res, next);
	}

	/**
	 * @api {delete} /products/:id Destroy a product
	 * @apiVersion 1.0.0
	 * @apiName Destro product
	 * @apiGroup Product
	 * @apiDescription Destroy a product
	 * @apiPermission admin
	 *
	 */
	destroy(req, res, next) {
		super.destroy(req, res, next);
	}
}

module.exports = ProductController;