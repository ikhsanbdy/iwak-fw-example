'use strict';

/**
 *
 *
 */

const Validator = use('Validator');
const Response 	= use('Response');

const Paginator = use('/app/libraries/Paginator');

const Category 	= use('/app/models/Category');

class CategoryController {

	/**
	 * Constructor
	 */
	constructor() {

	}

	/**
	 * @api {get} /categories Category list
	 * @apiVersion 1.0.0
	 * @apiName ListCategory
	 * @apiGroup Category
	 * @apiDescription Read all category list
	 *
	 */
	index(req, res, next) {
		try {
			const rules = {
				'page': 'integer',
				'page_size': 'integer',
				'order': `in:created_at,-created_at`,
				'id': 'match:^[\\d\\,]+$'
			};

			var Input = Validator.query(req, rules);

			if(Input.error()) {
				return Response.unprocessableEntity(res, 422001, 'Validation error', Input.messages());
			}
		}
		catch(err) {
			return next(err);
		}

		var Model = Category.forge();

		if(Input.get('id')) {
			Model.query((qb) => {
				qb.whereIn('id', Input.get('id').split(',').filter(Number))
			});
		}

		Model.orderBy(Input.get('order', 'created_at'))
		.fetchPage({
			page: Input.get('page', 1),
			pageSize: Input.get('page_size', 10)
		})
		.then((data) => {
			return Response.ok(res, Paginator.build(data));
		})
		.catch((err) => {
			return next(err);
		});
	}

	/**
	 * @api {get} /categories/:id Category detail
	 * @apiVersion 1.0.0
	 * @apiName DetailCategory
	 * @apiGroup Category
	 * @apiDescription Read category detail
	 *
	 */
	show(req, res, next) {
		Category.forge(req.params).fetch({withRelated: ['products.discount']})
		.then((data) => {
			if(!data) return Response.notFound(res, 404001, 'Not found');

			return Response.ok(res, data);
		})
		.catch((err) => {
			return next(err);
		});
	}

	/**
	 * @api {post} /categories Create category
	 * @apiVersion 1.0.0
	 * @apiName CreateCategory
	 * @apiGroup Category
	 * @apiDescription Create new category
	 * @apiPermission admin
	 *
	 */
	store(req, res, next) {
		try {
			var rules = {
				'name': 'required|string|min:3',
				'code': 'required|string|min:3',
				'activated': 'required|boolean'
			};

			var Input = Validator.body(req, rules);

			if(Input.error()) {
				return Response.unprocessableEntity(res, 422001, 'Validation error', Input.messages());
			}
		}
		catch(err) {
			return next(err);
		}

		new Category().save(Input.get())
		.then((data) => {
			return Response.created(res, data);
		}).catch((err) => {
			return next(err);
		});
	}

	/**
	 * @api {put} /categories/:id Update category
	 * @apiVersion 1.0.0
	 * @apiName Updatecategory
	 * @apiGroup Category
	 * @apiDescription Update category
	 * @apiPermission admin
	 *
	 *
	 */
	update(req, res, next) {
		try {
			var rules = {
				'name': 'string|min:3',
				'code': 'string|min:3',
				'activated': 'boolean'
			};

			var Input = Validator.body(req, rules);

			if(Input.error()) {
				return Response.unprocessableEntity(res, 422001, 'Validation error', Input.messages());
			}
		}
		catch(err) {
			return next(err);
		}

		Category.forge(req.params).save(Input.get())
		.then((data) => {
			return Response.ok(res, data);
		}).catch((err) => {
			if(err.message == 'No Rows Updated') return Response.notFound(res, 404001, 'Not found');

			return next(err);
		});
	}

	/**
	 * @api {delete} /categories/:id Destroy a category
	 * @apiVersion 1.0.0
	 * @apiName DestroyCategory
	 * @apiGroup Category
	 * @apiDescription Destroy a category
	 * @apiPermission admin
	 *
	 */
	destroy(req, res, next) {
		Category.forge(req.params).destroy()
		.then((data) => {
			return Response.ok(res, data);
		}).catch((err) => {
			return next(err);
		});
	}
}

module.exports = CategoryController;