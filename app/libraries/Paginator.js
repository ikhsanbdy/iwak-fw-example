'use strict';

/**
 *
 *
 */

class Paginator {

    static build(data) {
        try {
            let result = {
                page: data.pagination.page,
                page_size: data.pagination.pageSize,
                page_count: data.pagination.pageCount,
                row_count: data.pagination.rowCount,
                rows: data.toJSON()
            };

            return result;
        }
        catch(err) {
            throw err;
        }
    }
}

module.exports = Paginator;