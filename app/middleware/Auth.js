'use strict';

/**
 *
 *
 */

const Response  = use('Response');

class Auth {

	/**
	 *
	 *
	 */
	handle(req, res, next) {
		if(req.user) {
			return next();
		}
		else {
			return Response.unauthorized(res, 401001, 'Authentication required');
		}
	}
}

module.exports = Auth;