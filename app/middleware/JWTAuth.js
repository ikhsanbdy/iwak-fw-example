'use strict';

/**
 *
 *
 */

const jwt 		= use('jsonwebtoken');
const Response  = use('Response');

class JWTAuth {

	/**
	 *
	 *
	 */
	handle(req, res, next) {
		var accessToken = req.header('Authorization');

		if(accessToken) {
			try {
				accessToken = accessToken.split(' ');
				req.user = jwt.verify(accessToken[1], env('TOKEN_SECRET'));
			}
			catch(err) {
				return Response.unauthorized(res, 401002, 'Unathorized', [err]);
			}
		}

		return next();
	}
}

module.exports = JWTAuth;