'use strict';

/**
 *
 *
 */

/**
 * Load iwak-framework
 */
const app = require('iwak-framework');

/**
 * Load all route files
 */
require('fs').readdirSync('./app/routes').forEach(function (file) {
	require(`./app/routes/${file}`);
});

/**
 * Load http config
 */
require('./config/http')(app);

/**
 * Load boot config
 */
require('./config/boot');