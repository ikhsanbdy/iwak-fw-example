// Update with your config settings.

require('dotenv').config({silent: true});

module.exports = {

  development: {
    client: process.env['DB_CLIENT'],
    connection: {
      host:     process.env['DB_HOST'],
      port:     process.env['DB_PORT'],
      database: process.env['DB_DATABASE'],
      user:     process.env['DB_USERNAME'],
      password: process.env['DB_PASSWORD']
    },
    pool: {
      min: process.env['DB_MIN_CONNECTION'],
      max: process.env['DB_MAX_CONNECTION']
    },
    migrations: {
      tableName: 'migrations',
      directory: './database/migrations'
    },
    seeds: {
      directory: './database/seeds'
    }
  },

  staging: {
    client: process.env['DB_CLIENT'],
    connection: {
      host:     process.env['DB_HOST'],
      port:     process.env['DB_PORT'],
      database: process.env['DB_DATABASE'],
      user:     process.env['DB_USERNAME'],
      password: process.env['DB_PASSWORD']
    },
    pool: {
      min: process.env['DB_MIN_CONNECTION'],
      max: process.env['DB_MAX_CONNECTION']
    },
    migrations: {
      tableName: 'migrations',
      directory: './database/migrations'
    },
    seeds: {
      directory: './database/seeds'
    }
  },

  production: {
    client: process.env['DB_CLIENT'],
    connection: {
      host:     process.env['DB_HOST'],
      port:     process.env['DB_PORT'],
      database: process.env['DB_DATABASE'],
      user:     process.env['DB_USERNAME'],
      password: process.env['DB_PASSWORD']
    },
    pool: {
      min: process.env['DB_MIN_CONNECTION'],
      max: process.env['DB_MAX_CONNECTION']
    },
    migrations: {
      tableName: 'migrations',
      directory: './database/migrations'
    },
    seeds: {
      directory: './database/seeds'
    }
  }

};
