
exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('categories', function(table) {
            table.increments('id').primary().unsigned();
            table.string('code').unique().notNull();
            table.string('name').notNull();
            table.boolean('activated').notNull().default(true);

            table.integer('created_by').unsigned().nullable();
	        table.integer('updated_by').unsigned().nullable();
	        table.integer('deleted_by').unsigned().nullable();
	        
            table.timestamps(true);
            table.timestamp('deleted_at').nullable();
        }),
	]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('categories'),
	]);
};
