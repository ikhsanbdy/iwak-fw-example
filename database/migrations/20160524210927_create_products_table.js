
exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('products', function(table) {
            table.increments('id').primary().unsigned();
            table.string('code').unique().notNull();
            table.string('name').notNull();
            table.decimal('price', 15, 2).notNull();
            table.text('description').nullable();
            table.integer('stock_number').nullable();
            
            table.integer('category_id').unsigned().notNull();
            table.integer('discount_id').unsigned().nullable();
            table.boolean('activated').notNull().default(true);

            table.integer('created_by').unsigned().nullable();
            table.integer('updated_by').unsigned().nullable();
            table.integer('deleted_by').unsigned().nullable();

            table.timestamps(true);
            table.timestamp('deleted_at').nullable();

            table.foreign('category_id').references('id').inTable('categories');
            table.foreign('discount_id').references('id').inTable('discounts');
        }),
	]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('products'),
	]);
};
