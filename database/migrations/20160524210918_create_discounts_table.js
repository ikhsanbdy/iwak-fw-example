
exports.up = function(knex, Promise) {
	return Promise.all([
		knex.schema.createTable('discounts', function(table) {
            table.increments('id').primary().unsigned();
            table.string('name').notNull();
            table.integer('amount_percent').unsigned().notNull();
            table.boolean('activated').notNull().default(true);
            table.timestamp('expire_at').notNull();

            table.integer('created_by').unsigned().nullable();
            table.integer('updated_by').unsigned().nullable();
            table.integer('deleted_by').unsigned().nullable();

            table.timestamps(true);
            table.timestamp('deleted_at').nullable();
        }),
	]);
};

exports.down = function(knex, Promise) {
	return Promise.all([
		knex.schema.dropTable('discounts'),
	]);
};
