'use strict';

/**
 * App configuration
 *
 */

const session = use('express-session');

// const JWTAuth = use('/app/middleware/JWTAuth');

module.exports = function (app) {

	/**
	 * Session config
	 */
	app.set('trust proxy', 1) // trust first proxy
	app.use(session({
		secret: 'keyboard cat',
		resave: true,
		saveUninitialized: true,
		cookie: { secure: false }
	}));

	/**
	 * Allow origin
	 *
	 */
	app.use(function(req, res, next) {
		res.setHeader('Access-Control-Allow-Origin', '*');
    	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    	res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
		
		return (req.method === 'OPTIONS') ? res.end() : next();
	});

	// app.use(new JWTAuth().handle);

};