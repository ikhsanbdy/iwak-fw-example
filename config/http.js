'use strict';

/**
 * Http(s) configuration
 *
 */

const http	 = use('http');

module.exports = function (app) {
	
	/**
	 * Http server
	 *
	 */
	const server = http.Server(app);

	server.listen(env('APP_PORT', 3000), function () {
	    use('Logger').info(`${env('NODE_ENV', 'development').toUpperCase()} server running at port: ${env('APP_PORT', 3000)}`);
	});

};