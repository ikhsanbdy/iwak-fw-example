'use strict';

/**
 * Database configuration
 *
 */

module.exports = function (bookshelf) {
	
	/**
	 * Add bookshelf plugins here
	 *
	 */
	bookshelf.plugin('visibility');
	bookshelf.plugin('pagination');
	bookshelf.plugin('registry');
	bookshelf.plugin(require('bookshelf-paranoia'));

};